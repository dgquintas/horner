#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <vector>
#include <omp.h>
#include <cmath>
#include <cassert>
#include <utility>
#include <cstdio>
#include <ctime>

#include <gmpxx.h>

std::vector< std::pair<size_t, size_t> > partition(const size_t n, const size_t m){
    /** Returns a vector of pairs. Each pair defines the initial and final index
     * of the coefficient's vector to be considered by each of the m sections. 
     *
     * Each pair encompasses a evenly distributed amount of indices. 
     *
     * */
    std::vector< std::pair<size_t, size_t> > res;

    const size_t w = (n-1)/m + 1;
    
    for( size_t i = 0; i < n; i = std::min(n, i+w) ){
        res.push_back( std::pair<size_t, size_t>(i,std::min(n, i + w)) );
    } 

    return res;
} 

/** Naive polynomial evaluation given all the powers of x */
double evalPoly( const std::vector<double>& p, const double x0, 
                 const std::vector<double>& xs,
                 const size_t a=0, size_t b=0 ){
    if( b == 0 ){
        b = p.size();
    }

    double res = 0.0;
#pragma omp parallel for default(shared) reduction(+:res)
    for(int i=a; i < b; i++){
        res += p[i] * xs[i];
    }
    return res;
}

/** Post: xs[i] = x0^i */
void getSuccesivePowers(const double x0, std::vector<double>& xs){
    const size_t n = xs.size();
    xs[0] = 1.0;
    for( int i=1; i < n; i++){
        xs[i] = x0 * xs[i-1];
    }
}
/** Naive polynomial evaluation. */
double evalPoly( const std::vector<double>& p, const double x0 ){
    const size_t n = p.size();
    std::vector<double> xs(n);
    getSuccesivePowers(x0, xs);
    return evalPoly(p, x0, xs);
}


///////////////// HORNER STUFF ////////////////////
/** Iterative version.
 *
 * Evaluates @arg p for @arg x0 for the coefficients between
 * @arg a (lowest coefficient) and @arg b.
 *
 * @param p the polynomial.
 * @param x0 the value we are evaluating it for.
 * @param a index (wrt @arg p) of the constant
 * @param b index (wrt @arg p) of the highest order coefficient
 * */
mpf_class hornerIter( const mpf_class* const p, const mpf_class x0, 
                      const size_t a, const size_t b ){
    mpf_class res(x0);
    res = 0.0;
    for( int i = b-1; i > a; i--){
        res = (res + p[i])*x0;
    }
    return res + p[a];
}

/** Parallel version. 
 *
 * Each individual section uses hornerIter.
 *
 * @param p: polynomial's coefficients
 * @param n: polynomial's degree
 * @param x0: x's value
 * @param m: number of sections
 */
mpf_class pHorner(const mpf_class* const p, 
                  const size_t n, 
                  mpf_class x0, 
                  const size_t m ){
    std::vector<mpf_class> hs(m+1, mpf_class(x0) );
    const size_t w((n-1)/m + 1); // ceil(n/m), width of each of the m sections
    const std::vector< std::pair<size_t, size_t> > parts(partition(n, m));
#pragma omp parallel for default(shared) 
    for( int k = 0; k < parts.size(); k++){
        hs[k] = hornerIter(p, x0, parts[k].first, parts[k].second);
    }

    mpf_t tmp; mpf_init2(tmp, x0.get_prec());
    mpf_pow_ui(tmp, x0.get_mpf_t(), w); x0 = mpf_class(tmp); // x0 ^= w
    
    const mpf_class res( hornerIter( &(hs[0]), x0, 0, parts.size()) );

    return res;
}
///////////////// HORNER STUFF END ////////////////////

void printPartitions( const std::vector< std::pair<size_t, size_t> >& parts ){
    for( int i =0; i < parts.size(); i++){
        printf("(%lu, %lu) ", parts[i].first, parts[i].second);
    }
    printf("\n");
}
void printHs( double* hs, size_t n ){
    for( int i =0; i < n; i++){
        printf("%lf ", hs[i]);
    }
    printf("\n");
}

#define PREC 4096

int main(int argc, char** argv){
    srand( time(NULL) );
    srand( 0 );
    size_t n; // = 1000000;
    int m;// = omp_get_max_threads();
    if( argc < 3 ){
        fprintf(stderr, "./%s <pol. degree> <num. sections>\n", argv[0]);
        return 1;
    }
    n = atoi( argv[1] );
    m = atoi( argv[2] ); assert( m > 0 );

    std::cerr << "Using " << omp_get_max_threads() << " threads" << std::endl;

  //  std::vector<mpf_class> p(n);
    std::vector<mpf_class> p(n, mpf_class(0,PREC) ); // do NOT remove the second argument. It affects how the operator= works for the p[i]'s: if not specified, a default mpf_class will be created, with the default precision (~57 places)
    //  for( int i = 0; i < n; i++){
    //    p[i] = rand()/((double)RAND_MAX);
    //  }
    //  double x0 = rand()/((double)RAND_MAX);

    mpf_class ci(1, PREC);
    mpf_class one(1, PREC);
    p[0] = one;
    for( int i = 1; i < n; i++){
        ci *= (one/i);
        p[i] = ci ;
    }
    mpf_class x0(1, PREC);

    mpf_class res(0, PREC);
    double t1, t2;
    const int ITERS = 1000;

    double tHorner, tpHorner;

    //const double epsilon=0.0000001;
    //  assert( fabs(evalPoly(p, x0)-pHorner(&(p[0]), p.size(), x0, m)) < epsilon );
    //  assert( fabs(evalPoly(p, x0)-hornerIter(&p[0], x0, 0, p.size())) < epsilon );
    //
    //  std::cerr << std::setprecision(16) << evalPoly(p, x0) << std::endl;
    //  std::cerr << std::setprecision(16) << pHorner(&(p[0]), p.size(), x0, m) << std::endl;


    t1 = omp_get_wtime();
    for(int i=0; i<ITERS; i++){
        res = hornerIter(&p[0], x0, 0, p.size());
    }
    t2 = omp_get_wtime();
    tHorner = t2-t1;
    std::cerr << "horner: " << tHorner << "  sec" << std::endl;
    res = hornerIter(&p[0], x0, 0, p.size());
    gmp_printf( "%.Ff\n", res.get_mpf_t() );


    t1 = omp_get_wtime();
    for(int i=0; i<ITERS; i++){
        res = pHorner(&p[0], p.size(), x0, m);
    }
    t2 = omp_get_wtime();
    tpHorner = t2-t1;
    std::cerr << "P horner: " << tpHorner << "  sec" << std::endl;
    res = pHorner(&p[0], p.size(), x0, m);
    gmp_printf( "%.Ff\n", res.get_mpf_t() );
 

    //  std::cerr << "parallel horner: " << tpHorner << "  sec" << std::endl;
    //  std::cerr << res << std::endl;
    //
    //  const size_t w( (n-1) / m +1 );
    //  const double speedup=tHorner/tpHorner;
    //  const double eff=speedup/omp_get_max_threads();
    //
    //  printf("%f %f %d %f %d %f\n", 
    //      eff, speedup, omp_get_max_threads(), (m+0.0)/omp_get_max_threads(), w, w*sizeof(double)/1024.0);
    //
    //  std::cout << "Speedup = " << speedup << std::endl;
    //  std::cout << "Efficiency = " << eff << std::endl;

    return 0;

}



