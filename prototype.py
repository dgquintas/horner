import operator
import random
import timeit 
import math 
import pdb

def getSuccesivePowers(x0, n):
  xs = [1] #0th power
  xs.extend([x0]*(n-1))
  for i in xrange(1,n):
    xs[i] *= xs[i-1]

  return xs

def evalPoly(p, x0):
  global xs
  ai_x_xi = map( operator.mul, p, xs )
  res = sum( ai_x_xi )
  return res

def horner(p, x0):
  if len(p) == 1:
    res = p[0]
  else:
    res = p[0] + x0 * horner(p[1:], x0)
  return res

def hornerIter(p, x0):
  res = 0;
  for pi in reversed(p[1:]):
    res = (res + pi)*x0
  return res + p[0]


#def phorner(p, x0, m):
#  ps = partition(p, m)
#  phs = map( lambda pi: xs[ pi[0] ]*hornerIter(pi[1], x0), ps )
#  res = sum(phs)
#  return res
 
def phorner(p, x0, m, DIVFACTOR=None):
  if not DIVFACTOR:
    DIVFACTOR = m/2
  # hs is a vector representing the coefficients of a polynomial of degree n/m
  # its "x0" is x0**w

  #pdb.set_trace()

  while len(p) > DIVFACTOR:
    w = (len(p) - 1) // m +1
    ps = partition(p, m) #aprox m cachos, de w elementos cada uno
#parallel 
    hs = map( lambda pi: hornerIter(pi[1], x0), ps )
#join
    print " n = %d, w = %d, m = %d, hs = %s " % (len(p), w, m, hs)
    #x0 = x0 ** w
    print "x0 pre = %d" % x0
    x0 = x0 ** w 
    print "x0 post = %d ( w = %d )" % (x0, w)
    m /= DIVFACTOR
    p = hs # el nuevo p tiene ~m elementos. el proximo w tendra pues ~m/m/2 = 2

  # 1 <= m < 2
  #return hs[0] + hs[1] * x0
  return hornerIter(p, x0)


def partition(p,m):
  res=[]
  n=len(p)
  w = (n-1)//m + 1;
  r = n % m

  i=0
  j=i+w

  for _ in xrange(m):
    res.append( (i, p[i:j]) ) 
    i = j
    j = i+w
    if i >= len(p):
      break
  else:
    if r:
      res.append( (i, p[i:i+r]) ) 

  return res


if __name__ == "__main__":
  randFunc = random.uniform
  #randFunc = lambda a,b: long(random.randint(a,b))
#  n = 50
#  p = [ randFunc(5,10) for i in xrange(n) ]
#  x0 = randFunc(5,10)

  p = range(1,20+1)
  x0 = 2
  n = len(p)

  global xs
  xs = getSuccesivePowers(x0, n)

  classic = evalPoly(p, x0)
  print classic
  h = horner(p, x0)
  hi = hornerIter(p, x0)
  ph = phorner(p, x0, 4, 2) 
  print ph
  #phs = [ phorner(p, x0, i) for i in xrange(1,30) ]

  for ph in phs:
    assert( str(ph) == str(classic) )

  print classic
  print h
  print hi
 
  assert str(classic) == str(h)  == str(hi)

  from timeit import Timer
  t = Timer("evalPoly(p, x0)", "from __main__ import evalPoly, p, x0")
  ITERS = 100000
  print "CLASSIC: ", float(ITERS)/t.timeit(ITERS), " iters per sec"

  t2 = Timer("horner(p, x0)", "from __main__ import horner, p, x0")
  print "HORNER: ", float(ITERS)/t2.timeit(ITERS), " iters per sec"

  t3 = Timer("hornerIter(p, x0)", "from __main__ import hornerIter, p, x0")
  print "HORNER ITER: ", float(ITERS)/t3.timeit(ITERS), " iters per sec"


  for i in xrange(1, 30):
    t3 = Timer("phorner(p, x0, %d)" % i, "from __main__ import phorner, p, x0")
    print "PHORNER(%d): " % i, float(ITERS)/t3.timeit(ITERS), " iters per sec"

