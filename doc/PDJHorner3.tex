\documentclass{article}
\usepackage{amssymb}

%  Include here your own macros.
\usepackage[english]{babel}
\usepackage{pslatex}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{subfigure}
\usepackage{caption}
\usepackage{authblk}
\DeclareMathOperator{\lengthx}{length}
\def\length#1{ \lengthx{ \left(#1 \right) } }
\DeclareMathOperator{\speedupx}{speedup}
\def\speedup#1{ \speedupx{ \left(#1 \right) } }
\usepackage{graphicx}
  \graphicspath{{plots/}}
\usepackage[section]{algorithm}
\usepackage{algpseudocode}
\floatname{algorithm}{algorithm}
% End of your macros
\newcommand{\Eqref}[1]{(\ref{#1})}
\newtheorem{definition}{Definition}
\newtheorem{remark}{Remark}

\newcommand{\RN}[1]{%
  \textup{\uppercase\expandafter{\romannumeral#1}}%
}

\begin{document}

%  Full title. Use '\\' to force a line break.

\title{A Simple Parallelization of Horner's Algorithm for Polynomial Evaluation}


%  Full author information.
\author[1]{Policarpo Abascal Fuentes\thanks{abascal@uniovi.es}}
\author[2]{David García Quintas\thanks{dgq@google.com}}
\author[1]{Jorge Jiménez Meana\thanks{meana@uniovi.es}}
\affil[1]{Department of Mathematics, University of Oviedo}
\affil[2]{Google Inc.}

\renewcommand\Authands{and }

\maketitle
%  Abstract, key words and MSC codes.
\begin{abstract}
Horner's algorithm efficiently solves the problem of evaluating a
polynomial of degree $n$ in time $O(n)$. We present a simple parallelization of
this method that results in futher temporal gains.

{\bf Keywords:} parallel polynomial evaluation, parallel algorithms

{\bf AMS (MOS) subject clasification.} 68C25, 68C05, 68B10
\end{abstract}

\newpage
%
%  Main text of the article.
%

\section{Introduction}

Polynomial evaluation has been studied extensively due to its many applications.
For instance, the usage of polynomials with a high degree for Coding Theory
(cf.~\cite{MW}).  Error correcting codes are ubiquitous and their importance
only increases as demand for higher speeds in computing naturally tend to
introduce errors. Any improvement in the support algorithms is of great
interest.

Using parallel computing to address this problem has been examined in
in~\cite{D}, where a parallel implementation of the Horner algorithm can be
found. However, a readily available and straightforward implementation is not
presented there. Another example can be found in~\cite{BM}.

The main idea behind the method presented in this paper is the partitioning of
the polynomial in order to take advantage of multiple computational units
working in parallel.

This paper is organized as follows. Section~\ref{sec:2} introduces Horner's
algorithm for polynomial evaluation.  Section~\ref{sec:3} presents some
partitioning techniques that will allow us to work in parallel improving over
the traditional sequential algorithm.

Section~\ref{sec:4} presents the performance results and compares them against
the traditional Horner's method.

Conclusions will be presented in Section~\ref{sec:5}.

\section{Horner's Algorithm}\label{sec:2}
A polynomial is defined as an expression of the form
$a_0 + a_1x + \cdots + a_{n}x^n$. Scalars $a_i$ are called \emph{coefficients},
usually real or complex numbers; $x$ is considered as a variable: that is,
substituting an arbitrary number $\alpha$ for $x$, a well-defined number
$a_0+a_1\alpha+\cdots+a_n\alpha^n$ is obtained.
The arithmetic of polynomials is governed by the usual rules for the sum and
product operations.
The concept of polynomial and the associated operations can be generalized to a
formal algebraic setting in a straightforward manner.

\begin{definition}
Let $R$ be an arbitrary ring. A polynomial over $\mathbb{R}$ is an expression
of the form

$$ p(x) = \sum_{i=0}^{n}a_{i}x^i = a_0 + a_1x + \cdots + a_{n}x^n$$

where $n$ is a nonnegative integer, coefficients $a_i$ are
elements of $\mathbb{R}$, and $x$ is a symbol not belonging to $\mathbb{R}$,
called an indeterminate over $\mathbb{R}$.
\end{definition}

Evaluation using the monomial form of an $n$ degree polynomial requires at most
$n$ additions and $(n^2 + n)/2 = \mathcal{O}(n^2)$ multiplications if powers are
calculated by repeated multiplications and each
monomial is evaluated individually.

By contrast Horner's method evaluates $p$ at a point $x_0$ as follows:
$$p(x) = a_0 + ((a_1 + \cdots + (a_{n-1} + a_{n}x_0)x_0)\cdots)x_0$$

\begin{algorithm}
    \begin{algorithmic}[1]
        \caption{Horner's Method}\label{alg:horner}
        \Function{Horner}{coefficients $a_0, \ldots, a_n$, $x_0 \in \mathbb{R}$}
            \State $result \gets 0$
            \For{$i = n \to 1$}
                \State $result = (result + a_i) \cdot x_0$
                    \Comment{$n-1$ additions, $n-1$ products.}
            \EndFor
            \State $result = result + a_0$
            \State \Return\ $result$
                \Comment{$result \equiv p(x_0) = a_0 + a_1 x + \cdots + a_n x^n$}
        \EndFunction\
    \end{algorithmic}
\end{algorithm}

It performs $n$ additions and $n-1 = \mathcal{O}(n)$ multiplications,
requiring half the space the naive implementation does (the latter
not only needs to store the result, which is $\mathcal{O}(n\log_2{(x_0)})$
in space, but also the powers of $x_0$, with ${x_0}^n$ dominating).

Horner's method is optimal for the evaluation of arbitrary polynomials
inasmuch any other method would perform at least the same number of
operations (see~\cite{K}).

In certain cases the algorithm can also be used if $x$ is a matrix, in
which case the gain in computational efficiency is even greater.

\section{Polynomial Partitioning}\label{sec:3}
The idea of partitioning has already been used by the authors in the
parallelization of methods for error correcting codes (see~\cite{PDJ}).
By adequately partitioning the polynomial into sub-polynomials it becomes
possible to evaluate the latter independently, thereby in parallel.

\subsection{Preliminaries}\label{sec:3.1}
Let $p(x)=\sum_{i=0}^{n}a_{i}x^i$ be a polynomial over $\mathbb{R}$, $x_0 \in
\mathbb{R}$ the evaluation point.


\subsection{Partitioning Method}
The simplest partitioning mechanism would be to dividide $p(x)$ into chunks of
equal number of coefficients. If the data type of the coefficients is of
constant size (such as \texttt{float} or \texttt{double}), that would indeed
result in equally-sized subproblems. However, if precision-preserving types are
used for the coefficients, their sizes will vary with their complexity.

A better way to partition the vector of coefficients of varying sizes is to have
a maximum byte size per partition, linearly placing the dividing boundaries in
such a way that the sum of the coefficient sizes for the partition be less than
said maximum byte size. This uses the subproblem size as a proxy for its
computational cost. In addition, by choosing the maximum subproblem byte size to
be approximately equal to one of the CPU caches, it paves the way to exploiting
the different speeds of the usual memory hierarchies of a comodity computer, the
type with which this article is concerned.

If $p(x)=\sum_{i=0}^{n}a_{i}x^i$ is partitioned as follows:
$$
p(x) = \underbrace{a_0 + \cdots + a_4 x_0^4}_\RN{1} +
       \underbrace{a_5 x_0^5 + a_6 x_0^6}_\RN{2} +
       \underbrace{a_7 x_0^7 + a_8 x_0^8 + a_9 x_0^9}_\RN{3} + \cdots
$$

Each of $\RN{1}$, $\RN{2}$, etc.\ can be evaluated in parallel.

\subsection{Subproblem Evaluation}
Note that every subproblem is, like the original one, a \emph{dense} polynomial.
Each is evaluated using the iterative Horner algorithm described
in~\ref{alg:horner}.

After evaluating the subproblems, we need to combine them to form the final
solution:
$$
p(x_0) = \RN{1}x_0 + \RN{2}x_0^5 + \RN{3}x_0^7 + \cdots
$$
Note that $x_0$ ought to be raised to the index (with respect to $v_p$) of its
lower order coefficient, as it is straightforward to verify.

At this point we are confronted with the evaluation of yet another
polynomial, only in this case it is \emph{sparse}.

\subsection{Sparse Polynomial Evaluation}
The traditional Horner method assumes all or most of the polynomial coefficients
are non-zero, iterating over them sequentially in increments of one over the
coefficient indices. That is to say, the $x_0$ exponent difference between consecutive
coefficients taken into account is a constant one.

The moment a significant amount of coefficients are zero, considering them in
the computation is wasteful. By ignoring them, we now calculate the exponent
difference for the $x_0$ of every consecutive pair of non-zero coefficients. For
example, $p(x_0) = a_0 + a_1 x^5 + a_2 x^7 +a_3 x^{10}$ results in the following
sequence of exponent differences: $(0, 5, 2, 3)$. By raising $x_0$ to each of
the differences, we arrive at $(x_0^0, x_0^5, x_0^2, x_0^3)$, which are now used
in a slightly modified version of Horner's method:

\begin{algorithm}
    \begin{algorithmic}[1]
        \caption{Modified Horner for sparse polynomials}\label{alg:horner_sparse}
        \Function{HornerSparse}
            {coefficients $a_0, \ldots, a_n$,
             $x_0 \in \mathbb{R}$,
             differences $d_0, \ldots, d_n$}

            \State $result \gets 0$
            \ForAll{$d \in (d_0, \ldots, d_n)$}
            \State $powers[i] \gets x_0^d$ \Comment{$O[n(d \log x_0)^k]$, for
            mult.\ in $O(m^d)$ for an $m$ bit number.}
            \EndFor
            \For{$i = n \to 0$}
                \State $result = (result + a_i) \cdot powers[i]$
                    \Comment{$n-1$ additions, $n-1$ products.}
            \EndFor
            \State \Return $result$
        \EndFunction
    \end{algorithmic}
\end{algorithm}

Continuing with the example, the coefficients $(a_0, a_1, a_2, a_3)$ are
combined with $(x_0^0, x_0^5, x_0^2, x_0^3)$, $x_0$ to the exponent differences,
as follows, in the usual Horner fashion:
$$
\left[  \left[   \left[  a_3 x_0^3 + a_2 \right] x_0^2 + a_1 \right] x_0^5 + a_0
\right] x_0^0 = a_0 + a_1 x_0^5 + a_2 x_0^7 + a_3 x_0^{10}
$$

\subsubsection{Parallelization}
Consider now a sparse polinomial such that the size of its coefficients exceeds
the desired size threshold:

$$
p(x_0) = \underbrace{a_0 + a_1 x^5 + a_2 x^7 +a_3 x^{10}}_\text{Range $[0,3]$} +
         \underbrace{a_4 x^{12} + a_5 x^{15}}_\text{Range $[4,5]$} + \cdots
$$

where the partitions are calculated in the same way as before. Each partition
can once again be evaluated in parallel as described, and lastly combined by
adding them all together.


\section{Results}\label{sec:4}
% XXX

\section{Conclusions}\label{sec:5}
% XXX

\section*{Acknowledgements}


\begin{thebibliography}{99}
\bibitem{PDJ}{\sc P. Abascal Fuentes, D. García Quintas, J. Jiménez Meana},
{\em Improvements on binary coding using parallel computing}, {Int. J. Comput. Math.},  88(9),
{1896-1908}, {(2011)}.

\bibitem{D}{\sc M. l. Dowling},
{\em A fast parallel Horner algorithm}, {SIAM J. Comput.}, vol. 1,
{133--142}, {(1990)}.

\bibitem{BM}{\sc A. Borodin and I. Munro},
{\em The Computational Complexity of Algebraic and Numeric
Problems}, {American Elsevier, New York}, {1975}.

\bibitem{K} {\sc D. Knuth}, {\em The Art of Computer Programming}, Vol. 2:
Seminumerical Algorithms, Third Edition. Addison-Wesley, 1997. ISBN
0--201--89684--2.

\bibitem{MW}{\sc N. J. A. Sloane and F. J. MacWilliams},
{\em The Theory of error-correcting codes}, {North-Holland}, {1988}.
\end{thebibliography}
\end{document}


%\begin{thebibliography}{99}

%\bibitem{ABA}{\sc P.~Abascal and J.~Tena},
%{\em  Algoritmos de b\'{u}squeda de un c\'{o}digo corrector de errores
%realizando una estructura de acceso para compartir secretos},
%V Reuni\'{o}n Espa\~{n}ola de Criptolog\'{\i}a y Seguridad de la
%Informaci\'{o}n, pp. 279-288, ISBN: 84-8497-820-6, 1998.
%
%
%\bibitem{datasheet5100}{\sc Intel Corp.},
%{\em Quad-Core Intel Xeon Processor 5100 Series},
%{August}, {2007}, {http://download.intel.com/design/Xeon/datashts/31335503.pdf}
%
%
%\bibitem{datasheet5400}{\sc Intel Corp.},
%{\em Quad-Core Intel Xeon Processor 5400 Series},
%{August}, {2008}, {http://download.intel.com/design/xeon/datashts/318589.pdf}
%
%
%\bibitem{c}{\sc B. Kernighan and D. Ritchie},
%{\em The C Programming Language},
%{Prentice Hall}, {1988}.
%
%
%\bibitem{parpatterns}
%{\sc T. G. Mattson, B. A. Sanders and B. L. Massingill},
%{\em Patterns for Parallel Programming},
%{Addison-Wesley}, {2004}, {Software Patterns Series}.
%
%
%\bibitem{ME}{\sc R. J. McEliece},
%{\em A public-key cryptosystem based on algebraic coding theory.},
%{DSN Prog. Rep., Jet Prop. Lab., California Inst. Technol., Pasadena,
%CA}, {(1987)}, {114--116}.
%
%
