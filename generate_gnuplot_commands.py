from __future__ import print_function

def build_w(dfile, n, ws):
    indexed_data_pattern = ('"{dfile}" index "Single VS multi thread (n = {n}, w = {w})"'
                            ' using 3:6 with lp title "({n}, {w})"')
    args = [indexed_data_pattern.format(dfile=dfile, w=w, n=n) for w in ws]
    return 'plot [:][0:1] ' +  ', '.join(args)

if __name__ == '__main__':
    import sys
    dfile = sys.argv[1]
    ns = sys.argv[2].split(',')
    ws = sys.argv[3].split(',')

    #print('set term png small size 1200,800')
    for i, n in enumerate(ns):
        print('set term x11 persist', i)
        print('set grid')
        print('set xlabel "Num. Threads"')
        print('set ylabel "Efficiency"')
        print('set output "plots/%s-%s.png"' % (dfile, n))
        print(build_w(dfile, n, ws))


