import csv
import sys
from collections import defaultdict
from operator import itemgetter


def GetTable(r):
    table = defaultdict(lambda: defaultdict(list))
    items_f = itemgetter(0, 1, 2, 3, 4, 5)
    for line in r:
        if not line or line[0].lstrip().startswith('#'):
            continue
        n, w, t, t_ms, sup, eff = items_f(line)
        n = int(n)
        w = int(w)
        t = int(t)
        t_ms = float(t_ms)
        sup = float(sup)
        eff = float(eff)
        table[n][w].append((sup, t, eff))
    return dict(table)


if __name__ == '__main__':
    fname = sys.argv[1]
    t = None
    with open(fname) as f:
        r = csv.reader(f)
        t = GetTable(r)

    for n, ws in sorted(t.items()):
        for w, effs in sorted(ws.items()):
            print(n, w, max(effs))
