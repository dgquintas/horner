\documentclass{article}
\usepackage{amssymb}

%  Include here your own macros.
\usepackage[english]{babel}
\usepackage{pslatex}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{caption,subcaption}
\usepackage{authblk}
\DeclareMathOperator{\lengthx}{length}
\def\length#1{ \lengthx{ \left(#1 \right) } }
\DeclareMathOperator{\speedupx}{speedup}
\def\speedup#1{ \speedupx{ \left(#1 \right) } }
\usepackage{graphicx}
  \graphicspath{{figures/}}
\usepackage[section]{algorithm}
\usepackage{algpseudocode}
\floatname{algorithm}{algorithm}
% End of your macros
\newcommand{\Eqref}[1]{(\ref{#1})}
\newtheorem{definition}{Definition}
\newtheorem{remark}{Remark}
\begin{document}

\title{A Simple Parallelization of Horner's Algorithm for Polynomial Evaluation}

%  Full author information.
\author[1]{Policarpo Abascal Fuentes\thanks{abascal@uniovi.es}}
\author[2]{David Garc�a Quintas\thanks{dgquintas@gmail.com}}
\author[1]{Jorge Jim�nez Meana\thanks{meana@uniovi.es}}
\affil[1]{Department of Mathematics, University of Oviedo}
\affil[2]{Google Inc.}

\renewcommand\Authands{and }

\maketitle
%  Abstract, key words and MSC codes.
\begin{abstract}
Horner's algorithm efficiently solves the problem of evaluating a
polynomial of degree $n$ in time $O(n)$. We present a simple parallelization of
this method that results in futher temporal gains.

{\bf Keywords:} parallel polynomial evaluation, parallel algorithms

{\bf AMS (MOS) subject clasification.} 68C25, 68C05, 68B10
\end{abstract}

\newpage
%
%  Main text of the article.
%

\section{Introduction}

Polynomial evaluation has been studied extensively due to 
its many applications. For instance, the usage of 
polynomials with a high degree for Coding Theory (cf.~\cite{MW}).
Error correcting codes are ubiquitous and their importance only increases
as demand for higher speeds in computing naturally tend to introduce errors. 
Any improvement in the support algorithms is of great interest.

Using parallel computing to address this problem has been examined in
in~\cite{D}, where a parallel implementation of the Horner algorithm
can be found. However, a readily available and straightforward
implementation is not presented there. Another example can be found in~\cite{BM}. 

The main idea behind the method presented in this paper is the
partitioning of the polynomial in order to take advantage
of multiple computational units working in parallel. 

This paper is organized as follows. Section~\ref{sec:2} 
introduces Horner's algorithm for polynomial evaluation. 
Section~\ref{sec:3} presents some partitioning techniques that 
will allow us to work in parallel improving over the traditional
sequential algorithm. 

Section~\ref{sec:4} presents the performance results and compares 
them against the traditional Horner's method. 

Conclusions will be presented in Section~\ref{sec:5}.

\section{Horner's Algorithm}\label{sec:2}
A polynomial is defined as an expression of the form
$a_0+a_1x+\cdots +a_{n}x^n$. Scalars $a_i$ are called \emph{coefficients}, 
usually real or complex numbers; $x$ is considered as a variable: that is,
substituting an arbitrary number $\alpha$ for $x$, a well-defined number
$a_0+a_1\alpha+\cdots+a_n\alpha^n$ is obtained.
The arithmetic of polynomials is governed by the usual rules for the sum and
product operations.
The concept of polynomial and the associated operations can be generalized to a
formal algebraic setting in a straightforward manner.

\begin{definition}
Let $R$ be an arbitrary ring. A polynomial over $\mathbb{R}$ is an expression
of the form

$$ p(x) = \sum_{i=0}^{n}a_{i}x^i = a_0 + a_1x + \cdots + a_{n}x^n$$

where $n$ is a nonnegative integer, coefficients $a_i$ are
elements of $\mathbb{R}$, and $x$ is a symbol not belonging to $\mathbb{R}$,
called an indeterminate over $\mathbb{R}$.
\end{definition}

Evaluation using the monomial form of an $n$ degree polynomial
requires at most $n$ additions and $(n^2 + n)/2 = \mathcal{O}(n^2)$
multiplications if powers are calculated by repeated multiplications and each
monomial is evaluated individually.

By contrast Horner's method evaluates $p$ at a point $x_0$ as follows: 
$$p(x) = a_0 + ((a_1 + \cdots + (a_{n-1} + a_{n}x_0)x_0)\cdots)x_0$$

\begin{algorithm}
    \caption{Horner's Method}
    \begin{algorithmic}[1]
        \Function{Horner}{coefficients $a_0, \ldots, a_n$, $x_0 \in \mathbb{R}$}
            \State\ $result \gets 0$
            \For{$i = n \to 1$}
                \State\ $result = (result + a_i) \cdot x_0$
                    \Comment\ $n-1$ additions, $n-1$ products.
            \EndFor\
            \State\ $result = result + a_0$
            \State\ \Return\ $result$ 
                \Comment{$result \equiv p(x_0) = a_0 + a_1 x + \cdots + a_n x^n$}
        \EndFunction\
    \end{algorithmic}
\end{algorithm}

It performs $n$ additions and $n-1 = \mathcal{O}(n)$ multiplications, 
requiring half the space the naive implementation does (the latter
not only needs to store the result, which is $\mathcal{O}(n\log_2{(x_0)})$
in space, but also the powers of $x_0$, with ${x_0}^n$ dominating). 

Horner's method is optimal for the evaluation of arbitrary polynomials 
inasmuch any other method would perform at least the same number of
operations (see~\cite{K}).
%XXX: ponemos esto o no? pa que vale?
In certain cases the algorithm can also be used if $x$ is a matrix, in
which case the gain in computational efficiency is even greater. 

\section{Polynomial Partitioning}\label{sec:3}

The idea of partitioning has already been used by the authors in the 
parallelization of methods for error correcting codes (see~\cite{PDJ}).
By adequately partitioning the polynomial into sub-polynomials it becomes
possible to evaluate the latter independently, thereby in parallel. 

\subsection{Preliminaries}\label{sec:3.1}
From now on, we will assume that indices start at zero as well as the following:

\begin{description}
    \item[$p(x)$] the polynomial of degree $n$ being evaluated.
    \item[$\vec{v}$] the vector comprised of the coefficients $p(x)$.
    \item[$\vec{v_i}$] vectors whose components are the coefficients of polynomials
    $p_i(x)$ resulting from the partitioning.
    %\item[$(\vec{v}',\vec{v}'')$] the vector whose components are the juxtaposition
    %of the components of both vectors. 
\end{description}

\subsection{Partitioning Method}\label{sec:3.2}

Let $p(x)=\sum_{i=0}^{n}a_{i}x^i$ be a polynomial over
$\mathbb{R}$, $x_0\in \mathbb{R}$ the point where we want to evaluate the
polynomial and $m = \left|{\vec{v}}\right|$ the desired number of partitions.

\begin{itemize}
\item We construct the vector $\vec{v}$ from the coefficients in $p(x)$.
\item We split $\vec{v}$ into $\vec{v_1}, \vec{v_2}, \ldots, \vec{v_m}$
    consecutive component of $\vec{v}$.
\item The first $m-1$ vectors, $\vec{v_i}$ for $i < m$, have length 
    $w = \lceil \frac{n+1}{m} \rceil$. The last one, $\vec{v_m}$, 
    has length $r = (n+1) \bmod m$.
\end{itemize}

The construction of these vectors is straighforward: 

\begin{equation*}
    \vec{v_i} = \left\{ 
        \begin{array}{rl}
            a_{\left(i-1\right)w}, \ldots, a_{iw} & \text{if } i < m,\\
            a_{\left(i-1\right)w}, \ldots, a_{r}  & \text{if } i = m.
        \end{array} \right.
\end{equation*}

This partitioning is trivially performed in $w = \mathcal{O}(n/w)$ steps.

\subsection{Evaluation Process}\label{sec:3.3}
Let the values of $\vec{v_i}$ conform the coefficients of a polynomial $p_i(x)$. 
By considering $y = x^w$, we can rewrite the original polynomial $p(x)$ as:

$$  p(x) = p_1(x) + p_2(x)y + p_3(x)y^2 + \cdots + p_m(x)y^{m-1} $$

Each of the $p_i(x)$ subpolynomials can be evaluated in parallel. Except for 
the last one, they are all $w$-degree polynomials, meaning each subproblem 
is expected to require the about the same amount of computation. 

The last step, combining the results for the $m$ subproblems, is reduced 
to the evaluation of a $m$-degree polynomial at $y = x^w$. Naturally, if $m$
were large enough, the partitioning and parallel evalution method could be
applied again.  Likewise for the actual evaluation of the $p_i(x)$
subpolynomials. 

In any event, once the degree of the subproblem is small enough, the
base case evaluation is performed by means of the usual iterative version of
Horner's method. 

\section{Comparing the methods}\label{sec:4}

\subsection{Speedup over Iterative Method}
It is worth comparing the performance of the newly developed method against the
classic iterative algorithm. For this comparison, results for a single-thread
execution of the new method are considered. This is interesting inasmuch
speedups are still achieved prior to the introduction of any parallelism. This
is attributable to a better utilization of the caches for the resolution of
subproblems, whereby a subproblem fitting within a cache will exploit the
locallity principle.

Consider figure~\ref{fig:stvsiter}. The speedup is significative for polynomials
of degree greater than ... 

\begin{figure}
\centering
\includegraphics[width=.95\textwidth]{parallel_vs_iter_speedup.pdf}
\caption{Iterative method vs single-threaded.}
\label{fig:stvsiter}
\end{figure}


\subsection{Efficiency}

\begin{figure}
\makebox[\linewidth][c]{%
\begin{subfigure}[b]{.8\textwidth}
\centering
\includegraphics[width=.95\textwidth]{1000.pdf}
\caption{a test subfigure}
\end{subfigure}%
\begin{subfigure}[b]{.8\textwidth}
\centering
\includegraphics[width=.95\textwidth]{4000.pdf}
\caption{a test subfigure}
\end{subfigure}%
}\\
\makebox[\linewidth][c]{%
\begin{subfigure}[b]{.8\textwidth}
\centering
\includegraphics[width=.95\textwidth]{6000.pdf}
\caption{a test subfigure}
\end{subfigure}%
\begin{subfigure}[b]{.8\textwidth}
\centering
\includegraphics[width=.95\textwidth]{8000.pdf}
\caption{a test subfigure}
\end{subfigure}%
}
\caption{Efficiency for $N \in \{1000, 4000, 6000, 8000\}$}
\end{figure}

\subsubsection{Optimal values}
\begin{figure}
\makebox[\linewidth][c]{%
\begin{subfigure}[b]{.8\textwidth}
\centering
\includegraphics[width=.95\textwidth]{max_eff.pdf}
\caption{a test subfigure}
\end{subfigure}%
\begin{subfigure}[b]{.8\textwidth}
\centering
\includegraphics[width=.95\textwidth]{max_eff_idx.pdf}
\caption{a test subfigure}
\end{subfigure}%
}
\caption{Efficiency for $N \in \{1000, 4000, 6000, 8000\}$}
\end{figure}
\section{Conclusions}\label{sec:5}
% XXX

\section*{Acknowledgements}

The research reported on in this paper has been partially supported
by Project FEDER-MEC-MTM2007--61193.

\begin{thebibliography}{99}
\bibitem{PDJ}{\sc P. Abascal, D. Garc�a and J. Jimenez},
{\em Improvements on binary coding using parallel computing}, {Ni ComPUTA Idea}, vol. 1,
{??}, {(20??)}.

\bibitem{D}{\sc M. l. Dowling},
{\em A fast parallel Horner algorithm}, {SIAM J. Comput.}, vol. 1,
{133--142}, {(1990)}.

\bibitem{BM}{\sc A. Borodin and I. Munro},
{\em The Computational Complexity of Algebraic and Numeric
Problems}, {American Elsevier, New York}, {1975}.

\bibitem{K} {\sc D. Knuth}, {\em The Art of Computer Programming}, Vol. 2:
Seminumerical Algorithms, Third Edition. Addison-Wesley, 1997. ISBN
0--201--89684--2.

\bibitem{MW}{\sc N. J. A. Sloane and F. J. MacWilliams},
{\em The Theory of error-correcting codes}, {North-Holland}, {1988}.
\end{thebibliography}
\end{document}


%\begin{thebibliography}{99}

%\bibitem{ABA}{\sc P.~Abascal and J.~Tena},
%{\em  Algoritmos de b\'{u}squeda de un c\'{o}digo corrector de errores
%realizando una estructura de acceso para compartir secretos},
%V Reuni\'{o}n Espa\~{n}ola de Criptolog\'{\i}a y Seguridad de la
%Informaci\'{o}n, pp. 279-288, ISBN: 84-8497-820-6, 1998.
%
%
%\bibitem{datasheet5100}{\sc Intel Corp.},
%{\em Quad-Core Intel Xeon Processor 5100 Series},
%{August}, {2007}, {http://download.intel.com/design/Xeon/datashts/31335503.pdf}
%
%
%\bibitem{datasheet5400}{\sc Intel Corp.},
%{\em Quad-Core Intel Xeon Processor 5400 Series},
%{August}, {2008}, {http://download.intel.com/design/xeon/datashts/318589.pdf}
%
%
%\bibitem{c}{\sc B. Kernighan and D. Ritchie},
%{\em The C Programming Language},
%{Prentice Hall}, {1988}.
%
%
%\bibitem{parpatterns}
%{\sc T. G. Mattson, B. A. Sanders and B. L. Massingill},
%{\em Patterns for Parallel Programming},
%{Addison-Wesley}, {2004}, {Software Patterns Series}.
%
%
%\bibitem{ME}{\sc R. J. McEliece},
%{\em A public-key cryptosystem based on algebraic coding theory.},
%{DSN Prog. Rep., Jet Prop. Lab., California Inst. Technol., Pasadena,
%CA}, {(1987)}, {114--116}.
%
%
